from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor

import os

# запуск бота LongPolling
token = '5146294840:AAFETGoOnj7D9yBiTPYRK1go_TlXMNQYwf4'
bot = Bot(token = token)
dp = Dispatcher(bot)

# Функция ответа (асинхрон)
@dp.message_handler()
async def echo_send(message: types.Message):
    # ответ пользавателю
    await message.answer(message.text)

    # ответ на сообщение
    #await message.reply(message.text)
    
    # ответ в личку БОТ НЕ ПИШЕТ ПЕРВЫМ
    #await bot.send_message(message.from_user.id, message.text)


# При запусске бота, чтоб не засыпало сообщениями - Skip_updates = True
executor.start_polling(dp, skip_updates= True)


